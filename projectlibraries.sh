#!/bin/bash

apt-get -y update
apt-get install -y nginx
service nginx start
sudo apt install ffmpeg -y
sudo apt install openjdk-11-jre-headless -y
sudo apt install openjdk-11-jdk-headless -y
sudo curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y nodejs
